package com.qadeer.web.dao;

import com.qadeer.web.model.Student;

import java.util.*;
import java.sql.*;

public class StudentDao {

    public static Connection getConnection(){
        Connection con=null;
        try{
            Class.forName("com.mysql.cj.jdbc.Driver");
            con=DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb","root","root");
        }catch(Exception e){System.out.println(e);}
        return con;
    }
    public static int create(Student e){
        int status=0;
        try{
            Connection con= StudentDao.getConnection();
            PreparedStatement ps=con.prepareStatement(
                    "insert into student(name,rollno,age) values (?,?,?)");
            ps.setString(1,e.getName());
            ps.setInt(2,e.getRollno());
            ps.setInt(3,e.getAge());
            status=ps.executeUpdate();

            con.close();
        }catch(Exception ex){ex.printStackTrace();}

        return status;
    }
    public static int update(Student e){
        int status=0;
        try{
            Connection con= StudentDao.getConnection();
            PreparedStatement ps=con.prepareStatement(
                    "update student set name=?,rollno=?,age=? where id=?");
            ps.setString(1,e.getName());
            ps.setInt(2,e.getRollno());
            ps.setInt(3,e.getAge());
            ps.setInt(4,e.getId());

            status=ps.executeUpdate();

            con.close();
        }catch(Exception ex){ex.printStackTrace();}

        return status;
    }
    public static int delete(int id){
        int status=0;
        try{
            Connection con= StudentDao.getConnection();
            PreparedStatement ps=con.prepareStatement("delete from student where id=?");
            ps.setInt(1,id);
            status=ps.executeUpdate();

            con.close();
        }catch(Exception e){e.printStackTrace();}

        return status;
    }
    public static Student getStudentById(int id){
        Student e=new Student();

        try{
            Connection con= StudentDao.getConnection();
            PreparedStatement ps=con.prepareStatement("select * from student where id=?");
            ps.setInt(1,id);
            ResultSet rs=ps.executeQuery();
            if(rs.next()){
                e.setId(rs.getInt(1));
                e.setName(rs.getString(2));
                e.setRollno(rs.getInt(3));
                e.setAge(rs.getInt(4));
            }
            con.close();
        }catch(Exception ex){ex.printStackTrace();}

        return e;
    }
    public static Student getStudentByRollNo(int rollno){
        Student e=new Student();

        try{
            Connection con= StudentDao.getConnection();
            PreparedStatement ps=con.prepareStatement("select * from student where rollno=?");
            ps.setInt(1,rollno);
            ResultSet rs=ps.executeQuery();
            if(rs.next()){
                e.setId(rs.getInt(1));
                e.setName(rs.getString(2));
                e.setRollno(rs.getInt(3));
                e.setAge(rs.getInt(4));
            }
            con.close();
        }catch(Exception ex){ex.printStackTrace();}

        return e;
    }
    public static List<Student> getAllStudents(){
        List<Student> list=new ArrayList<Student>();

        try{
            Connection con= StudentDao.getConnection();
            PreparedStatement ps=con.prepareStatement("select * from student");
            ResultSet rs=ps.executeQuery();
            while(rs.next()){
                Student e=new Student();
                e.setId(rs.getInt(1));
                e.setName(rs.getString(2));
                e.setRollno(rs.getInt(3));
                e.setAge(rs.getInt(4));
                list.add(e);
            }
            con.close();
        }catch(Exception e){e.printStackTrace();}

        return list;
    }
}